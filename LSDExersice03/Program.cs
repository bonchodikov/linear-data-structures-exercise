﻿using System;
using System.Collections.Generic;

namespace LSDExersice04
{
    class Program
    {
        static void Main()
        {
            var arr = Console.ReadLine().Split(' ');
            var dict = new Dictionary<string, List<int>>();
            foreach (var item in arr)
            {
                if (dict.ContainsKey(item))
                {
                    dict[item].Add(1);
                }
                else
                {
                    dict.Add(item, new List<int> { 1 });
                }
            }
            
            foreach (var item in dict)
            {
                var assrr = dict[item.Key].ToArray();                
                Console.WriteLine($"{item.Key} {assrr.Length} times");              
            }
        }
    }
}
