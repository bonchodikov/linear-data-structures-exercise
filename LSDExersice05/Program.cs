﻿using System;
using System.Collections.Generic;

namespace LSDExersice05
{
    class Program
    {
        static void Main(string[] args)
        {
            var arr = Console.ReadLine().Split(' ');
            var dict = new Dictionary<string, List<int>>();
            foreach (var item in arr)
            {
                if (dict.ContainsKey(item))
                {
                    dict[item].Add(1);
                }
                else
                {
                    dict.Add(item, new List<int> { 1 });
                }
            }
            int result = 0;
            string str="";
            foreach (var item in dict)
            {
                if (item.Value.Count>result)
                {
                    result = item.Value.Count;
                    str = item.Key;
                }
            }
            Console.WriteLine(str);
        }
    }
}
