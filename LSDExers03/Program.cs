﻿using System;
using System.Collections.Generic;

namespace LSDExers03
{
    class Program
    {
        static void Main()
        {
            var arr = Console.ReadLine().Split(' ');
            var dict = new Dictionary<string, List<int>>();
            foreach (var item in arr)
            {
                if (dict.ContainsKey(item))
                {
                    dict[item].Add(1);
                } 
                else
                {
                    dict.Add(item, new List<int> { 1 });
                }
            }
            for (int i = 0; i < arr.Length; i++)
            {
                if (dict.ContainsKey(arr[i]))
                {
                    if (dict[arr[i]].Count%2==0)
                    {
                        Console.Write(arr[i]+" ");
                    }
                }
            }
            Console.WriteLine();
            
        }
    }
}
