﻿using System;
using System.Collections;

namespace LSDExersice06
{
    class Program
    {
        static void Main()
        {
            string input = Console.ReadLine();
            var stack = new Stack();
            bool isGood = true;
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i]=='(')
                {
                    stack.Push('(');
                }
                if (input[i] == ')')
                {
                    try
                    {
                        stack.Pop();
                    }
                    catch (Exception)
                    {
                        isGood = false;
                        Console.WriteLine("do not match");
                        break;
                    }                    
                }
            }
            if (isGood)
            {
                if (stack.Count == 0)
                {
                    Console.WriteLine("do match");
                }
                else
                {
                    Console.WriteLine("do not match");
                }
            }
        }
    }
}
