﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LSDExersice02
{
   public class Sequence<T>
    {
        public static IEnumerable<T> LongestSubs(T[] arr)
        {
            var result = new List<T>();
            int n = 0;
            
            for (int zi = 0; zi < arr.Length; zi++)
            {
                int tmp = 1;
                var final = new List<T>();
                final.Add(arr[zi]);
                for (int i = zi; i < arr.Length - 1; i++)
                {
                    if (arr[i].Equals(arr[i + 1]))
                    {
                        final.Add(arr[i]);
                        tmp++;
                    }
                    else
                    {
                        break;
                    }
                }
                if (tmp>n)
                {
                    n = tmp;
                    result = final;                    
                }
                continue;
            }

            return result as IEnumerable<T>;
            
        }
    }
}
