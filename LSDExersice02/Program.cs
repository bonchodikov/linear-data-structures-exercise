﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LSDExersice02
{
    class Program
    {
        static void Main()
        {
            var list = new List<string>(Console.ReadLine().Split(' ').ToArray());
            var result = Sequence<string>.LongestSubs(list.ToArray());
            foreach (var item in result)
            {
                Console.WriteLine(item);
            }



        }
    }
}
