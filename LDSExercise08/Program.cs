﻿using System;
using System.Collections.Generic;

namespace LDSExercise08
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            var listRes = new List<int>(50);
            listRes.Add(n);
            listRes.Add(n + 1);
            listRes.Add(2 * n + 1);
            listRes.Add(n + 2);
            for (int i = 1; i < 50; i++)
            {
                listRes.Add(listRes[i] + 1);
                if (listRes.Count == 50)
                {
                    Console.WriteLine(string.Join(", ", listRes));
                    break;
                }
                listRes.Add(listRes[i] * 2 + 1);
                listRes.Add(listRes[i] + 2);               
            }
        }
    }
}
