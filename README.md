Linear Data Structures Exercise

1. Write a program that reads N integers from the console and reverses them using a stack.

2. Write a method that finds the longest subsequence of equal numbers (consecutive) in given List and returns the result as new List<int>.

3. Write a program that removes from given sequence all numbers that occur odd number of times.


{4, 2, 2, 5, 2, 3, 2, 3, 1, 5, 2} → {5, 3, 3, 5}



4. Write a program that finds in given array of integers (all belonging to the range [0..1000]) how many times each of them occurs.


array = {3, 4, 4, 2, 3, 3, 4, 3, 2}

2 → 2 times
3 → 4 times
4 → 3 times




5. Write a program to find the majorant of given array (if exists). A majorant is a number that occurs at least n/2 + 1 times


{2, 2, 3, 3, 2, 3, 4, 3, 3} → 3



6. Write a program that check whether brackets match: (try to combine Push() and Pop() from Stack)


(1 + (2 * 3)) → match

1 + (2 * 3)) → do not match

(1 + )2 * 3)) → do not match

(1 + (2 * 3) → do not match

((((5 / 2) + 8) - 1 ) * 3) + 12 → match


7. Write a program that extracts sub-expressions. Print each expression on a new line. Assume expressions are always valid (brackets will match)


(1 + (2 * 3)) → sub-expressions are:

2 * 3
1 + (2 * 3)



((((5 / 2) + 8) - 1 ) * 3) + 12 → sub-expressions are:

5 / 2
(5 / 2) + 8
((5 / 2) + 8) - 1
(((5 / 2) + 8) - 1 ) * 3



(12 + 2) * (13 / 2) → sub-expressions are:

12 + 2
13 / 2



Hint: A stack will ensure that the index of the most recent opening bracket is always on top

8. We are given the following sequence:

S1 = N;
S2 = S1 + 1;
S3 = 2*S1 + 1;
S4 = S1 + 2;
S5 = S2 + 1;
S6 = 2*S2 + 1;
S7 = S2 + 2;
...

Using the Queue<T> class write a program to print its first 50 members for
given N.


N=2 → 2, 3, 5, 4, 4, 7, 5, 6, 11, 7, 5, 9, 6, ...