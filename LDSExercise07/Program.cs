﻿using System;
using System.Collections;

namespace LDSExercise07
{
    class Program
    {
        static void Main()
        {
            var str = Console.ReadLine();
            var stack = new Stack();
            int beg = 0;
            int end = str.Length;
            for (int i = beg; i < end; i++)
            {
                if (str[i] == '(')
                {
                    stack.Push(i);
                }
                else if(str[i] == ')')
                {
                    var h = stack.Pop();
                    int start = (int)h;
                    for (int iz = start+1; iz < i; iz++)
                    {
                        Console.Write(str[iz]);
                    }
                    Console.WriteLine();  
                }
            }
        }
    }
}
