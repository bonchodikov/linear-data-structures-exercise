﻿using System;
using System.Collections;

namespace LDSExercise01
{
    class Program
    {
        static void Main(string[] args)
        {
            //Write a program that reads N integers from the console and reverses them using a stack.
            int n = int.Parse(Console.ReadLine());
            var arr = new int[n];
            var stack = new Stack();
            for (int i = 0; i < n; i++)
            {
                stack.Push(int.Parse(Console.ReadLine()));
            }
            while (stack.Count>0)
            {
                string str = stack.Pop().ToString();
                for (int i = str.Length-1; i >=0; i--)
                {
                    Console.Write(str[i]);
                }
                Console.WriteLine();
            }
            

        }
    }
}
